const firstName = document.querySelector(".f_name")
const secondName = document.querySelector(".s_name")
const password = document.querySelector(".password")
const email = document.querySelector(".email")
const phone = document.querySelector(".phone")
const form = document.querySelector("form")





form.addEventListener("submit", (e) =>{
  const EMAIL_REGEXP = /^[^\s@]+@[^\s@]+\.[^\s@]+$/
  const PHONE_REGEXP = /^\+\d{1,3}\d{3,}$/

  let errorCount =0;

  
  if(firstName.value===""){
    errorCount+=1
    let fNameError = document.getElementById('fNameError')
    setErrorProperties(fNameError, firstName)
  } 

  if(secondName.value===""){
    errorCount+=1
    let sNameError = document.getElementById("sNameError")
    setErrorProperties(sNameError, secondName)
  }


  if(password.value.length<8){
    errorCount+=1
    let passwordError = document.getElementById("passError")
    setErrorProperties(passwordError, password)
  }


  if(!EMAIL_REGEXP.test(email.value) || email.value.length===0){
    errorCount+=1
    let mailError = document.getElementById("mailError")
    setErrorProperties(mailError, email)
  }


  if(!PHONE_REGEXP.test(phone.value) || phone.value.length===0){
    errorCount+=1
    let phoneError = document.getElementById("phoneError")
    setErrorProperties(phoneError, phone)
  }

  if(errorCount>0){
    e.preventDefault();
  }

})


function setErrorProperties(errorElement, inputElement){
  errorElement.style.visibility="visible"
    inputElement.style.borderColor="red"
}
